# guest_book
If you want to start the project with PHP's builtin server you can do the 
the following steps:
1. Update `config/config.php` file with your own credentials.
2. Import the database that is inside `phpdocker/database` to your database engine.
3. cd to `web`
4. Start the web server with php -S localhost:8000

If you want to start the project with docker you can just do this command:
`docker-compose up --build` and the process will run on the background
and you can access the page at: localhost:8001.Everything else is already set up.

